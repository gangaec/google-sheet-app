import googleSheets as sheets
import json
import signal
import argparse
import time

defaultConf = {"sheetID": "", 
                "columns": {
                    "A1": "Serial No", 
                    "B1": "Status"
                },
                "creds_path": "creds.json",
                "client_secret_path": "client_secret.json"
              }

APP_CONF_FILE = 'app-config.json'

# Save the configuration for next time
def saveConfig(config):
    with open(APP_CONF_FILE, 'w') as config_file:
        json.dump(config, config_file, sort_keys = True,
            indent = 4, ensure_ascii=False)

# Load the configuration for SheetID, columns
# and secret file
def loadConfig():
    try:
        with open(APP_CONF_FILE) as config_data:
            config = json.load(config_data)
        return config
    except Exception as e:
        saveConfig(defaultConf)
        return loadConfig()

def select_sheet(conf):
    creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])
    print("\t0 - Select a sheets from drive")
    print("\t1 - Create a new sheet in drive")
    ch = raw_input("Select an option to continue [0]: ")
    if ch == '1':
        sheetName = raw_input("\nEnter sheetname: ")
        sheetID = sheets.createSheet(creds, sheetName)
    elif ch == '0' or ch == '':
        sheetID = sheets.getSheetID(creds)
    else:
        print ch
        print 'Invalid Choise'
        exit(0)
    conf["sheetID"] = sheetID
    saveConfig(conf)
    return sheetID


def signal_term_handler(signal, frame):
    print('\n\nYou pressed Ctrl+C!')
    print('Exiting')
    sys.exit(0)
signal.signal(signal.SIGINT, signal_term_handler)

def main(args):
    customSheetId = None
    if args.sheet_id:
        customSheetId = args.sheet_id
    elif args.sheet_name:
        conf = loadConfig()
        creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])
        sheetsId = sheets.sheetIDFromName(creds, args.sheet_name)
        customSheetId = sheetsId

    if args.configure:
        conf = loadConfig()
        if customSheetId:
            saveConfig(customSheetId)
            print "Updated Sheet ID"
        else:
            select_sheet(conf)
        exit(0)

    elif args.empty and args.range:
        conf = loadConfig()
        if not conf["sheetID"]:
            print "No sheet selected. Please run --config first."
            exit(1)

        sheetID = conf["sheetID"]
        creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])
        sheets.clearCells(creds, sheetID, args.range)
        exit(0)

    elif args.range and args.append:
        conf = loadConfig()     

        if customSheetId:
            sheetID = customSheetId
        else:
            if not conf["sheetID"]:
                print "No sheet selected. Please run --config first."
                exit(1)
            sheetID = conf["sheetID"]
        creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])
        values = args.values.split(',')
        sheets.appendCells(creds, sheetID, args.range, [values])
        exit(0)

    elif args.range and args.values:
        conf = loadConfig()     

        if customSheetId:
            sheetID = customSheetId
        else:
            if not conf["sheetID"]:
                print "No sheet selected. Please run --config first."
                exit(1)
            sheetID = conf["sheetID"]
        creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])

        allCellRanges = args.range.split(' ')
        allCellValues = args.values.split(' ')
        data = []

        for indx, cellRange in enumerate(allCellRanges):
            cellRowValues = allCellValues[indx].split(';')
            # for column:
            if (':' in cellRange and cellRange[0] == cellRange.split(':')[1][0]):
                cellValues = []
                for value in cellRowValues[0].split(','):
                    cellValues.append([value])
            # for row or rectangle
            else:
                cellValues = [x.split(',') for x in cellRowValues]
            # print cellValues
            data.append({ 'range': 'sheet1!' + cellRange,
                    'values': cellValues })
        # print data
        sheets.batchUpdate(creds, sheetID, data)
        exit(0)
    
    elif args.interactive:
        conf = loadConfig()
        if customSheetId:
            sheetID = customSheetId
        else:
            if not conf["sheetID"]:
                print "No sheet selected. Please run --config first."
                exit(1)

        sheetID = conf["sheetID"]
        creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])

        cells = conf["columns"].keys()
        cellsFor = conf["columns"].values()
        cols = [ cell[0] for cell in cells ]
        prev_row = [ cell[1:] for cell in cells ]

        count = args.count
        if not count:
            inf_loop = True
        else:
            inf_loop = False
        while (inf_loop or count):
            if not inf_loop:
                count -= 1
            data = []
            for index, col in enumerate(cols):
                cellID = col + prev_row[index]
                val = raw_input("[" + cellID + "]\t" + cellsFor[index] + ":")
                time.sleep(1)
                prev_row[index] = str(int(prev_row[index])+1)
                data.append({"range": "sheet1!"+cellID, 'values': [[val]] })
            # print data
            sheets.batchUpdate(creds, sheetID, data)
            print ("")
    elif args.sheets_list:
        conf = loadConfig()
        creds = sheets.getCredentials(conf["creds_path"], conf["client_secret_path"])
        print json.dumps(sheets.sheetsList(creds), indent=2, sort_keys=True)
    else:
        print "Try --help."


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='42Q Demo - Google Sheets Cell Updating App')
    parser.add_argument('-c', '--configure', action='store_true', help='Configure the Google sheet')
    parser.add_argument('-r', '--range', help='Cell rage e.g: A3, D4:D6, B1:C2')
    parser.add_argument('-e', '--empty',  action='store_true', help='Clears the rage of cells. Use with "-r"')
    parser.add_argument('-a', '--append',  action='store_true', help='Appends the data to next free row in the given row')
    parser.add_argument('-v', '--values', help='Values that are going to update in specified range. Use with "-r". \n\
                                        eg: "cell-A3", "D4,D5,D6", "B1,B2;C1,C2"')
    parser.add_argument('-i', '--interactive', action='store_true', help="Interactive mode.")
    parser.add_argument('-n', '--count', help="No.of times that takes the input from user interactively. Use with option -i", type=int)
    parser.add_argument('--sheet-name', help='Sheet name to be used to update data. Use with --config or to update data')
    parser.add_argument('--sheet-id', help='Which SheetID to be used to update data. Use with --config or to update data')
    parser.add_argument('--sheets-list',action='store_true',help='List the sheets')


    args = parser.parse_args()
    main(args)