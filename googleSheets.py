from __future__ import print_function
import httplib2
import os
import signal

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

def signal_term_handler(signal, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_term_handler)

SCOPES = 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/spreadsheets'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


def getCredentials(credential_path, CLIENT_SECRET_FILE):
    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        print("Launching auth flow")
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        flags = tools.argparser.parse_args(args=[])
        credentials = tools.run_flow(flow, store, flags)
        print('Storing credentials to ' + credential_path)
    return credentials


def getSheetID(credentials):
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    page_token = None
    while True:
        # 
        response = service.files().list(q="mimeType='application/vnd.google-apps.spreadsheet'",
                                             spaces='drive',
                                             fields='nextPageToken, files(id, name)',
                                             pageToken=page_token).execute()
        print("\nSheets List")
        for file in response.get('files', []):
            # Process change
            print ('\t%s - \t%s' % (file.get('id'), file.get('name')))
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break;
    sheetID = raw_input("\nEnter a sheet ID to select: ")
    return sheetID

def sheetsList(credentials):
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    page_token = None
    sheets = {}
    while True:
        response = service.files().list(q="mimeType='application/vnd.google-apps.spreadsheet'",
                                             spaces='drive',
                                             fields='nextPageToken, files(id, name)',
                                             pageToken=page_token).execute()
        for file in response.get('files', []):
            # Process change
            sheets[file.get('id')] = file.get('name')
        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break;
    return sheets

def sheetIDFromName(credentials, name):
    sheets = sheetsList(credentials)
    ids = sheets.keys()
    names = sheets.values()
    if name in names:
        i = names.index(name)
        return ids[i]
    else:
        return None



def createSheet(credentials, sheetName):
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    body = {
      'mimeType': 'application/vnd.google-apps.spreadsheet',
      'name': sheetName,
    }

    newFile = service.files().create(body=body).execute()
    return newFile["id"]


def appendCells(credentials, sheetID, cellID, cellValues):
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    body = {
        "values": cellValues
    }

    spreadsheetId = sheetID
    rangeName = 'Sheet1!' + cellID
    result = service.spreadsheets().values().append(
        spreadsheetId=spreadsheetId, range=rangeName, valueInputOption='USER_ENTERED', body=body).execute()

def batchUpdate(credentials, sheetID, data):
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                                discoveryServiceUrl=discoveryUrl)
    
    body = {
      'valueInputOption': 'USER_ENTERED',
      'data': data
    }
    result = service.spreadsheets().values().batchUpdate(spreadsheetId=sheetID, body=body).execute()
    
def clearCells(credentials, sheetID, cellID):
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)
    body = {}
    spreadsheetId = sheetID
    rangeName = 'Sheet1!' + cellID
    result = service.spreadsheets().values().clear(
        spreadsheetId=spreadsheetId, range=rangeName, body=body).execute()
