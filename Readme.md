# Google Sheets App for 42Q

> <b>What's New</b>
>
> * Added `--append` option.
> * Added `--sheet-name` option
> * Added support for bactch updating cells
> * User can use `--sheet-name` or `--sheet-id` while adding a row


## Pre-requisites

1. Install Python 2.7
2. Install pip
3. pip install httplib2
4. pip install --upgrade google-api-python-client

## Configuring App

Open _app-config.json_ to configure the application

* `sheetID` - Which Google sheet has to be used to write the data. Leave it black if you don't know
* `client_secret_path` - Path to downloaded client_secret file from Google Developer Console. Helps to get authenticate user.
* `creds_path` - Once user completed the Oauth2 authentication, where to store credential details
* `columns` - What columns has to be filled in interactive mode.


## App Usage

```
usage: app.py [-h] [-c] [-r RANGE] [-e] [-a] [-v VALUES] [-i] [-n COUNT]
              [--sheet-name SHEET_NAME] [--sheet-id SHEET_ID] [--sheets-list]

42Q Demo - Google Sheets Cell Updating App

optional arguments:
  -h, --help            show this help message and exit
  -c, --configure       Configure the Google sheet
  -r RANGE, --range RANGE
                        Cell rage e.g: A3, D4:D6, B1:C2
  -e, --empty           Clears the rage of cells. Use with "-r"
  -a, --append          Appends the data to next free row in the given row
  -v VALUES, --values VALUES
                        Values that are going to update in specified range.
                        Use with "-r". eg: "cell-A3", "D4,D5,D6",
                        "B1,B2;C1,C2"
  -i, --interactive     Interactive mode.
  -n COUNT, --count COUNT
                        No.of times that takes the input from user
                        interactively. Use with option -i
  --sheet-name SHEET_NAME
                        Sheet name to be used to update data. Use with
                        --config or to update data
  --sheet-id SHEET_ID   Which SheetID to be used to update data. Use with
                        --config or to update data
  --sheets-list         List the sheets

```

## Running App

Examples for updating cells non-interactively

```
python ./app.py --range=A1 --values="This is A1 Cell"    # Single cell
python ./app.py -r A1:C1 -v "Cell-A1, Cell-B1, Cell-C1"  # Cells in a row
python ./app.py -r A1:B3 -v "A1,B1;A2:B2;A3,B3"          # Range of cells in rectangle
```

Examples for cleaning cells

```
python ./app.py --empty --range A1:A5
```

Examples for updating cells interactively

```
python ./app.py -i      # Asks infinite times. press ctrl+c to stop
python ./app.py -i -n4  # Asks 4 times and exit
```
Example for appending a row

```
python ./app.py -a -r A:B -v "value1,value2"
```
Batch updating cells:

```
python ./app.py -r "A1 F1 B2:D2" -v "cell-a1 cell-f1 cell-b2,cell-c2,c3ll-c3"
```

Configuring app with sheet name
```
python ./app.py --sheet-name "Demo for Al" -r A1:B3 -v "A1,B1;A2:B2;A3,B3"
```

Using app without configuring sheet:
```
python ./app.py --sheet-name "Demo for Al" -r A1:B3 -v "A1,B1;A2:B2;A3,B3"
```